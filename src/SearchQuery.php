<?php

namespace Nino\GoogleBooksSearch;

class SearchQuery
{
    public int $maxResults;
    public int $startIndex = 0;
    private string $queryString;
    private array $options;

    private const DEFAULT_OPTIONS = [
        // The maximum number of results to return, the maximum allowable value is 40.
        'maxResults' => 10,
        // Restrict search to a specific print type (all, books, magazines)
        'printType' => 'all',
        // Get certain fields from the (lite) or all the fields (full)
        'projection' => 'full',
        // Order of results (relevance, newest)
        'sorting' => 'relevance',
    ];

    public function __construct(string $queryString, array $options = [])
    {
        $this->queryString = $queryString;
        $this->options = array_merge(self::DEFAULT_OPTIONS, $options);
        $this->maxResults = $this->options['maxResults'];
    }

    public function getQueryArray(int $page = 0): array
    {
        $this->startIndex = $this->maxResults * $page;

        return array_merge($this->options, ['startIndex' => $this->startIndex, 'q' => $this->queryString]);
    }
}
