<?php

namespace Nino\GoogleBooksSearch;

class SearchResults
{
    public array $books;
    public bool $isLastPage;

    public function __construct($apiResponse, SearchQuery $query)
    {
        $this->books = isset($apiResponse->items) ? $apiResponse->items : [];
        $this->isLastPage = $query->startIndex + $query->maxResults > $apiResponse->totalItems;
    }
}
