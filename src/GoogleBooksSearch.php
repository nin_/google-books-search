<?php

namespace Nino\GoogleBooksSearch;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class GoogleBooksSearch
{
    private const API_URL = 'https://www.googleapis.com/books/v1/';
    private Client $http;
    private bool $autoPaginate = false;
    private int $page = 0;
    private array $globalOptions;
    private SearchQuery $query;

    /**
     * GoogleBooks constructor.
     * @param array $options default options for searches, can be overwritten at each searches.
     */
    public function __construct(array $options = [])
    {
        $this->http = new Client([
            'base_uri' => self::API_URL,
        ]);

        $this->globalOptions = $options;
    }

    /**
     * @param string $q https://developers.google.com/books/docs/v1/using#q
     * @param array $options
     * @return GoogleBooksSearch
     * @throws RequestException
     * @throws ClientException
     */
    public function search(string $q, array $options = []): self
    {
        $this->query = new SearchQuery($q, array_merge($this->globalOptions, $options));

        // reset the page count for auto pagination on a new search
        $this->page = 0;

        return $this;
    }

    /**
     * Increment the page on each run of the get method
     */
    public function autoPaginate(): self
    {
        $this->autoPaginate = true;

        return $this;
    }

    public function page(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function get(): SearchResults
    {
        $response = $this->http->request('GET', 'volumes', [
            'query' => $this->query->getQueryArray($this->page),
        ]);


        if ($this->autoPaginate()) {
            $this->page++;
        }

        return new SearchResults(json_decode($response->getBody()), $this->query);
    }
}
