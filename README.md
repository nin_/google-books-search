# php-google-books-search

PHP package to search volumes from [Google Books API](https://developers.google.com/books/docs/v1/reference/).

# Usage
## Installation

```bash
composer require nino/google-books-search
```

## Basic usage

```php
use Nino\GoogleBooksSearch\GoogleBooksSearch;

$booksSearch = new GoogleBooksSearch();

$result = $booksSearch->search('quilting')->get();

foreach ($result->books as $book) {
    echo "<h1>" . $book->volumeInfo->title . "</h1>";
    echo "<p>" . $book->volumeInfo->description . "</p>";
};
```

## Options

The search accept the following options :

- `maxResults` : The maximum number of results to return, the maximum allowable value is 40.
- `printType` : Restrict search to a specific print type (all, books, magazines)
- `projection` : Get certain fields from the (lite) or all the fields (full)
- `sorting` : Order of results (relevance, newest)

You can pass theses to the GoogleBooksSearch's constructor to set theses for all searches of this instance.

You can pass theses to the search method to overwrite instance's options.

```php
public function __construct(array $options = []);
public function search(string $q, array $options = []): self;
```

## Pagination
### Auto pagination

The method `autoPaginate()` allow you to get a new page at each `get()`.

```php
use Nino\GoogleBooksSearch\GoogleBooksSearch;

$booksSearch = new GoogleBooksSearch();

$booksSearch->search('quilting')->autoPaginate();

$page1 = $booksSearch->get();
$page2 = $booksSearch->get();
```

Note : If you redefine a search query via the `search()` method the page count is reset.

### Manual pagination

Set the page you want to get with the `page()` method prior to the `get()` call

```php
use Nino\GoogleBooksSearch\GoogleBooksSearch;

$booksSearch = new GoogleBooksSearch();

$page4 = $booksSearch->search('quilting')->page(4)->get();
$page8 = $booksSearch->page(8)->get();
```

### Is there a next page ?

If you want to know if there is a next page the result returned by the `get()` method have a property `isLastPage`

```php
use Nino\GoogleBooksSearch\GoogleBooksSearch;

$booksSearch = new GoogleBooksSearch();

$result = $booksSearch->search('quilting')->page(4)->get();
if ($result->isLastPage) {
    echo "last page: 4";
};
```

Note that `isLastPage()` return `true` if you have requested a page greater than the actual last page.

# Contributing

Merge requests, bug reports and suggestions are welcome.

# License
This library is licensed under the GNU Affero General Public License v3.0 or later.

Please read the LICENSE file for details.
